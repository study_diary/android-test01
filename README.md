# Android WebView demo

> 安卓通过WebView加载h5的简单例子。

## 快速开始

下载  然后用 Android Studio 打开，运行.

### 使用本地html资源

把h5资源文件放在 (包含 `index.html`，入口为`index.html`)  `assets` 的目录下。

## 调试查看

- 代码中设置：
  
> webView.setWebContentsDebuggingEnabled(true);

- 浏览器输入：
  
> chrome://inspect/#devices

找到设备-> 点击"inspect"
