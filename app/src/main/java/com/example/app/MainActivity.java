package com.example.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.example.app.wh.WriteHandlingWebViewClient;

public class MainActivity extends Activity {

    private WebView mWebView;

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = findViewById(R.id.activity_main_webview);
        WebSettings webSettings = mWebView.getSettings();
        // 设置与Js交互的权限
        webSettings.setJavaScriptEnabled(true);
        // 设置允许JS弹窗
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebViewClient(new WriteHandlingWebViewClient(mWebView));
        mWebView.addJavascriptInterface(this, "LinkNative"); // 注入Native对象到webview window中

        // debug
        mWebView.setWebContentsDebuggingEnabled(true);
        // file:// 跨域问题
        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);

        webSettings.setSupportZoom(true);// 是否可以缩放，默认true
        webSettings.setBuiltInZoomControls(true);// 是否显示缩放按钮，默认false
        webSettings.setUseWideViewPort(true);// 设置此属性，可任意比例缩放。大视图模式
        webSettings.setLoadWithOverviewMode(true);// 和setUseWideViewPort(true)一起解决网页自适应问题
        webSettings.setAppCacheEnabled(true);// 是否使用缓存
        webSettings.setDomStorageEnabled(true);// 开启本地DOM存储
        webSettings.setLoadsImagesAutomatically(true); // 加载图片
        webSettings.setMediaPlaybackRequiresUserGesture(false);// 播放音频，多媒体需要用户手动？设置为false为可自动播放
        webSettings.setAllowFileAccess(true); // 允许访问文件

        // REMOTE RESOURCE
//     mWebView.loadUrl("https://example.com");
        mWebView.loadUrl("https://sso.douyin.com/?service=https://www.douyin.com/home?scene=douyin_mobile");
//    mWebView.loadUrl("https://192.168.65.142:9033");
//        WebviewSettingProxy.setProxy(mWebView, "", 9033, "123");
//        mWebView.loadUrl("https://42.192.155.41:9033");
        // LOCAL RESOURCE
//    mWebView.loadUrl("file:///android_asset/www/site/index.html");
    }

    @JavascriptInterface
    public void jsonrpc(String param) {
        Log.i("[INFO]jsonrpc:", param.toString());
        nativeCallJs(param);
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public void nativeCallJs(String parameters) {
        String jsStr = "javascript:linkNativeCallBack(" + parameters + ")";
        Log.i("[INFO]nativeCallJs:", jsStr.toString());
        // js
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.evaluateJavascript(jsStr, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        // 此处为 js 返回的结果
                        Log.i("[INFO]onReceiveValue:", value.toString());
                    }
                });
            }
        });
    }
}
