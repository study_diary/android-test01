package com.example.app;

import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class MyWebViewClient extends WebViewClient {

    //    @Override
    //    public boolean shouldOverrideUrlLoading(WebView view, String url) {
    //        String hostname;
    //        // YOUR HOSTNAME
    //        hostname = "file:///android_asset/www/site/index.html";
    //        //"file:///android_asset/index.html" || "https://example.com"
    //        Uri uri = Uri.parse(url);
    //        if (url.startsWith("file:") || uri.getHost() != null && uri.getHost().endsWith(hostname)) {
    //            return false;
    //        }
    //        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    //        view.getContext().startActivity(intent);
    //        return true;
    //    }

    //WebviewClient 中重写这两个方法，拦截资源
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (request.getUrl().getPath().equals("/quick_login/v2/")) {
            Log.i("111", request.getClass().getName());
            return WebviewDnsInterceptUtil.getDnsInterceptRequest(view, request);
//            WebResourceResponse response = super.shouldInterceptRequest(view, request);
//            Log.i("111", response.getClass().getName());
        }
        return super.shouldInterceptRequest(view, request);
    }

    //    @Override
    //    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
    //        if (BuildConfig.RELEASE) {
    //            return super.shouldInterceptRequest(view, url);
    //        } else {
    //            return WebviewDnsInterceptUtil.getDnsInterceptUrl(view, url);
    //        }
    //    }

    //    @SuppressLint("WebViewClientOnReceivedSslError")
    //    @Override
    //    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
    //        handler.proceed();
    //    }

    //    public static void setKitKatWebViewProxy(Context appContext, String host, int port) {
    //        System.setProperty("http.proxyHost", host);
    //        System.setProperty("http.proxyPort", port + "");
    //        System.setProperty("https.proxyHost", host);
    //        System.setProperty("https.proxyPort", port + "");
    //        try {
    //            Class applictionCls = Class.forName("android.app.Application");
    //            Field loadedApkField = applictionCls.getDeclaredField("mLoadedApk");
    //            loadedApkField.setAccessible(true);
    //            Object loadedApk = loadedApkField.get(appContext);
    //            Class loadedApkCls = Class.forName("android.app.LoadedApk");
    //            Field receiversField = loadedApkCls.getDeclaredField("mReceivers");
    //            receiversField.setAccessible(true);
    //            ArrayMap receivers = (ArrayMap) receiversField.get(loadedApk);
    //            for (Object receiverMap : receivers.values()) {
    //                for (Object rec : ((ArrayMap) receiverMap).keySet()) {
    //                    Class clazz = rec.getClass();
    //                    if (clazz.getName().contains("ProxyChangeListener")) {
    //                        Method onReceiveMethod = clazz.getDeclaredMethod("onReceive", Context.class, Intent.class);
    //                        Intent intent = new Intent(Proxy.PROXY_CHANGE_ACTION);
    //
    //                        /*********** optional, may be need in future *************/
    //                        final String CLASS_NAME = "android.net.ProxyProperties";
    //                        Class cls = Class.forName(CLASS_NAME);
    //                        Constructor constructor = cls.getConstructor(String.class, Integer.TYPE, String.class);
    //                        constructor.setAccessible(true);
    //                        Object proxyProperties = constructor.newInstance(host, port, null);
    //                        intent.putExtra("proxy", (Parcelable) proxyProperties);
    //                        /*********** optional, may be need in future *************/
    //
    //                        onReceiveMethod.invoke(rec, appContext, intent);
    //                    }
    //                }
    //            }
    //        } catch (ClassNotFoundException e) {
    //            e.printStackTrace();
    //        } catch (NoSuchFieldException e) {
    //            e.printStackTrace();
    //        } catch (IllegalAccessException e) {
    //            e.printStackTrace();
    //        } catch (IllegalArgumentException e) {
    //            e.printStackTrace();
    //        } catch (NoSuchMethodException e) {
    //            e.printStackTrace();
    //        } catch (InvocationTargetException e) {
    //            e.printStackTrace();
    //        } catch (InstantiationException e) {
    //            e.printStackTrace();
    //        }
    //    }

}
