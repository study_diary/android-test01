package com.example.app;

import android.annotation.SuppressLint;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;

public class WebviewDnsInterceptUtil {

    @SuppressLint("NewApi")
    public static WebResourceResponse getDnsInterceptRequest(WebView view, WebResourceRequest request) {
        if (request != null && request.getUrl() != null) {
            return getWebResourceFromUrl(request.getUrl().toString());
        }
        return null;
    }

    //    public static WebResourceResponse getDnsInterceptUrl(WebView view, String url) {
    //        if (!TextUtils.isEmpty(url) && Uri.parse(url).getScheme() != null) {
    //            return getWebResourceFromUrl(url);
    //        }
    //        return null;
    //    }

    //核心拦截方法
    private static WebResourceResponse getWebResourceFromUrl(String url) {

//        String scheme = Uri.parse(url).getScheme().trim();

//        ZLog.d("web log 请求 url: " + url);

//        String ips = ProxyList.getList().get(Uri.parse(url).getHost());
//        if (StringUtils.isEmpty(ips)) {
//            ZLog.d("web log 不拦截：" + url);
//            return null;
//        }

        // HttpDns解析css文件的网络请求及图片请求
//        if ((scheme.equalsIgnoreCase("http") || scheme.equalsIgnoreCase("https"))) {
//        try {
        String oldUrl = url.replace("https://sso.douyin.com", "http://192.168.65.142:9033");
        oldUrl += "&oauth_id=123456";
        Log.w("代理链接", "\nold->" + url + "\nnew->" + oldUrl);
//                URLConnection connection = oldUrl.openConnection();
        // 获取HttpDns域名解析结果 // 通过HTTPDNS获取IP成功，进行URL替换和HOST头设置
//                ZLog.d("HttpDns ips are: " + ips + " for host: " + oldUrl.getHost());
//                String ip;
//                if (ips.contains(";")) {
//                    ip = ips.substring(0, ips.indexOf(";"));
//                } else {
//                    ip = ips;
//                }
//                String newUrl = url.replaceFirst(oldUrl.getHost(), ip);
//                ZLog.d("newUrl a is: " + newUrl);
//                connection =  new URL(newUrl).openConnection(); // 设置HTTP请求头Host域
//            URLConnection connection = new URL(oldUrl).openConnection();
//            connection.setRequestProperty("Origin", "sso.douyin.com");
//                ZLog.d("ContentType a: " + connection.getContentType());
//有可能是text/html; charset=utf-8的形式，只需要第一个
//                String[] strings = connection.getContentType().split(";");
//            OutputStream out = connection.getOutputStream();//向对象输出流写出数据，这些数据将存到内存缓冲区中
//            out.write("mix_mode=1&mobile=2e3d3325343d35343c333c3c313536&code=363c33333330&service=https%3A%2F%2Fwww.douyin.com%2F&fixed_mix_mode=1".getBytes(StandardCharsets.UTF_8));            //out.write(new String("测试数据").getBytes());            //刷新对象输出流，将任何字节都写入潜在的流中
//            out.flush();
        HttpResponse response = HttpUtil.createPost(oldUrl).body("mix_mode=1&mobile=2e3d3325343d35343c333c3c313536&code=363c33333330&service=https%3A%2F%2Fwww.douyin.com%2F&fixed_mix_mode=1").execute();

        return new WebResourceResponse("text/html", response.contentEncoding(), response.bodyStream());
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        }
//        return null;
    }
}