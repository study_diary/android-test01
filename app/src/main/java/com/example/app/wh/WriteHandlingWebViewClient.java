package com.example.app.wh;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;


public class WriteHandlingWebViewClient extends WebViewClient {

    private final String MARKER = "AJAXINTERCEPT";

    private Map<String, String> ajaxRequestContents = new HashMap<>();


    public WriteHandlingWebViewClient(WebView webView) {
        AjaxInterceptJavascriptInterface ajaxInterface = new AjaxInterceptJavascriptInterface(this);
        webView.addJavascriptInterface(ajaxInterface, "interception");
    }

    /*
    This here is the "fixed" shouldInterceptRequest method that you should override.
    It receives a WriteHandlingWebResourceRequest instead of a WebResourceRequest.
     */
    public WebResourceResponse shouldInterceptRequest(final WebView view, WriteHandlingWebResourceRequest request) {
        if (request.getUrl().getPath().equals("/")) {
            HttpResponse response = HttpUtil.createGet(request.getUrl().toString()).execute();
            return new WebResourceResponse("text/html", response.contentEncoding(), response.bodyStream());
        } else if (request.getUrl().getPath().equals("/quick_login/v2/")) {
            String oldUrl = request.getUrl().toString();
            // String newUrl = oldUrl.replace("https://sso.douyin.com", "http://192.168.65.142:9033");
            String newUrl = oldUrl.replace("https://sso.douyin.com", "http://42.192.155.41:9033");
            newUrl += "&oauth_id=123456";
            Log.w("代理链接", "\n\nold->" + oldUrl + "\nnew->" + newUrl);
            HttpResponse response = HttpUtil.createPost(newUrl).body(request.getAjaxData()).execute();
            return new WebResourceResponse("text/html", response.contentEncoding(), response.bodyStream());
        }
        //        else if (request.getUrl().getPath().equals("/send_activation_code/v2/")) {
        //            HttpResponse response = HttpUtil.createPost(request.getUrl().toString())
        //                    .contentType("application/x-www-form-urlencoded")
        //                    .body(request.getAjaxData()).execute();
        //            return new WebResourceResponse("text/html", response.contentEncoding(), response.bodyStream());
        //        }
        return null;
    }

    @Override
    public final WebResourceResponse shouldInterceptRequest(final WebView view, WebResourceRequest request) {
        String requestBody = null;
        Uri uri = request.getUrl();
        if (isAjaxRequest(request)) {
            requestBody = getRequestBody(request);
            uri = getOriginalRequestUri(request, MARKER);
        }
        WebResourceResponse webResourceResponse = shouldInterceptRequest(view, new WriteHandlingWebResourceRequest(request, requestBody, uri));
        if (webResourceResponse == null) {
            return null;
        } else if ("/quick_login/v2/".equals(request.getUrl().getPath())) {
            return webResourceResponse;
        }
        //        else if ("/send_activation_code/v2/".equals(request.getUrl().getPath())) {
        //            return webResourceResponse;
        //        }
        else {
            return injectIntercept(webResourceResponse, view.getContext());
        }
    }


    void addAjaxRequest(String id, String body) {
        ajaxRequestContents.put(id, body);
    }

    private String getRequestBody(WebResourceRequest request) {
        String requestID = getAjaxRequestID(request);
        return getAjaxRequestBodyByID(requestID);
    }

    private boolean isAjaxRequest(WebResourceRequest request) {
        return request.getUrl().toString().contains(MARKER);
    }

    private String[] getUrlSegments(WebResourceRequest request, String divider) {
        String urlString = request.getUrl().toString();
        return urlString.split(divider);
    }

    private String getAjaxRequestID(WebResourceRequest request) {
        return getUrlSegments(request, MARKER)[1];
    }

    private Uri getOriginalRequestUri(WebResourceRequest request, String marker) {
        Log.i("代理链接", "old url ->" + request.getUrl().toString());
        String urlString = getUrlSegments(request, marker)[0];
        Log.i("代理链接", "new url ->" + urlString);
        return Uri.parse(urlString);
    }

    private String getAjaxRequestBodyByID(String requestID) {
        String body = ajaxRequestContents.get(requestID);
        ajaxRequestContents.remove(requestID);
        return body;
    }

    private WebResourceResponse injectIntercept(WebResourceResponse response, Context context) {
        String encoding = response.getEncoding();
        String mime = response.getMimeType();
        // TODO WebResourceResponse的mime必须为"text/html",不能是"text/html; charset=utf-8"
        //        if (mime.contains("text/html")) {
        //            mime = "text/html";
        //        }
        InputStream responseData = response.getData();
        InputStream injectedResponseData = injectInterceptToStream(context, responseData, mime, encoding);
        return new WebResourceResponse(mime, encoding, injectedResponseData);
    }

    private InputStream injectInterceptToStream(Context context, InputStream is, String mime, String charset) {
        try {
            byte[] pageContents = Utils.consumeInputStream(is);
            if (mime.equals("text/html")) {
                String pageText = AjaxInterceptJavascriptInterface.enableIntercept(context, pageContents);
                // TODO 待验证
                //                pageContents = pageText.getBytes(charset);
                pageContents = pageText.getBytes(StandardCharsets.UTF_8);
            }
            return new ByteArrayInputStream(pageContents);
        } catch (Exception e) {
            Log.e("请求代理", e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
}